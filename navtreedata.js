/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Caleb O'Gorman's Mechatronics Portfolio", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "Senior Project", "index.html#sec_senior", null ],
    [ "Contact Info", "index.html#contact", null ],
    [ "Source Code", "index.html#source_code", null ],
    [ "Fibonacci Sequence", "page_fib.html", [
      [ "Introduction", "page_fib.html#fib_intro", null ],
      [ "Source Code", "page_fib.html#fib_src_code", null ]
    ] ],
    [ "Imaginary Elevator", "page_elevator.html", [
      [ "Introduction", "page_elevator.html#elevator_intro_", null ],
      [ "State Transition Diagram", "page_elevator.html#state_diagram", null ],
      [ "Source Code", "page_elevator.html#elevator_src_code", null ]
    ] ],
    [ "Blinking LED", "page_blinking.html", [
      [ "Introduction", "page_blinking.html#blinking_intro", null ],
      [ "State Transition Diagram", "page_blinking.html#blinking_diagram", null ],
      [ "Source Code", "page_blinking.html#led_src_code", null ]
    ] ],
    [ "Simon Taps", "simon_taps.html", [
      [ "Introduction", "simon_taps.html#simon_intro", null ],
      [ "Video Demonstration", "simon_taps.html#video_demo", null ],
      [ "State Transition Diagram", "simon_taps.html#simon_state", null ],
      [ "Source Code", "simon_taps.html#simon_src_code", null ]
    ] ],
    [ "ME 305 Term Project", "term_project.html", [
      [ "Project Introduction", "term_project.html#term_project_intro", null ],
      [ "Week 1: Serial-UART Communication", "term_project.html#week_1", [
        [ "Task Diagram", "term_project.html#week1_intro", null ],
        [ "Finite State Machines", "term_project.html#week1_fsms", null ],
        [ "Plotting on the CPU", "term_project.html#plotting_on_comp", null ]
      ] ],
      [ "Week 2: Encoders", "term_project.html#week_2", [
        [ "Encoder Driver", "term_project.html#encoder_driver_sec", null ],
        [ "Encoder Task", "term_project.html#encoder_task_sec", null ]
      ] ],
      [ "Week 3: Motors", "term_project.html#week_3", [
        [ "Motor Driver", "term_project.html#motor_driver", null ],
        [ "Control System", "term_project.html#control_system", [
          [ "Block Diagram", "term_project.html#block_diagram_sec", null ],
          [ "Transfer Function Analysis", "term_project.html#oltf_analysis", null ]
        ] ],
        [ "Task Diagram", "term_project.html#week3_task_diagram", null ]
      ] ],
      [ "Week 4: Reference Tracking", "term_project.html#week_4", [
        [ "Controller", "term_project.html#controller_class", null ],
        [ "Results", "term_project.html#week4_results", null ]
      ] ],
      [ "Source Code", "term_project.html#final_source_code", null ]
    ] ],
    [ "Vendotron", "page_vendo.html", [
      [ "Introduction", "page_vendo.html#vendo_intro", null ],
      [ "State Transition Diagram", "page_vendo.html#vendo_diagram", null ],
      [ "Source Code", "page_vendo.html#vendo_src_code", null ]
    ] ],
    [ "Think Fast", "page_think.html", [
      [ "Introduction", "page_think.html#think_Intro", null ],
      [ "State Transition Diagram", "page_think.html#think_fsm", null ],
      [ "Two Different Methods", "page_think.html#think_methods", null ],
      [ "Source Code", "page_think.html#think_src_code", null ]
    ] ],
    [ "Term Project Modeling", "page_modeling.html", [
      [ "Introduction", "page_modeling.html#hw0x02_intro", null ],
      [ "Hand Calculations", "page_modeling.html#hand_calcs", null ]
    ] ],
    [ "Simulation or Reality?", "page_simulation.html", [
      [ "Introduction", "page_simulation.html#sim_intro", null ],
      [ "Open Loop", "page_simulation.html#sim_open", null ],
      [ "Closed Loop", "page_simulation.html#sim_closed", null ],
      [ "Matlab Script", "page_simulation.html#sim_script", null ]
    ] ],
    [ "Pushing the Right Buttons", "page_pushing.html", [
      [ "Introduction", "page_pushing.html#pushing_intro", null ],
      [ "Time Constant", "page_pushing.html#lab3_time_const", null ],
      [ "Source Code", "page_pushing.html#push_src_code", null ]
    ] ],
    [ "Hot or Not?", "page_temp.html", [
      [ "Introduction", "page_temp.html#temp_intro", null ],
      [ "Results", "page_temp.html#temp_results", null ],
      [ "Source Code", "page_temp.html#temp_source", null ]
    ] ],
    [ "Full-State Feedback: Pole Placement", "page_placement.html", [
      [ "Introduction", "page_placement.html#placement_introduction", null ],
      [ "Results", "page_placement.html#placement_results", null ]
    ] ],
    [ "ME 405 Term Project", "term_405.html", [
      [ "Project Introduction", "term_405.html#t_405_intro", null ],
      [ "Hardware Setup", "term_405.html#hardware_setup", null ],
      [ "Resistive Touch Panel", "term_405.html#touch_Screen", [
        [ "Wiring", "term_405.html#touch_panel_wiring", null ],
        [ "Touch Panel Driver", "term_405.html#touch_panel_driver", null ],
        [ "Performance", "term_405.html#touch_panel_performance", null ]
      ] ],
      [ "Encoders", "term_405.html#tp_encoders", [
        [ "Encoder Driver", "term_405.html#tp_encoder_driver_sec", null ],
        [ "Encoder Task", "term_405.html#tp_encoder_task_sec", null ]
      ] ],
      [ "Motors", "term_405.html#tp_week_3", [
        [ "Motor Driver", "term_405.html#tp_motor_driver", null ],
        [ "Fault Detection", "term_405.html#fault_detection", null ]
      ] ],
      [ "Controller", "term_405.html#controller_tp", null ],
      [ "Source Code", "term_405.html#tp_source_code", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"Elevator_8py.html",
"index.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';