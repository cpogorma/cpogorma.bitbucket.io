var ascii__frontend_8py =
[
    [ "kb_cb", "ascii__frontend_8py.html#a5920e921c481b7e3b0c9f907bd6c6f6c", null ],
    [ "callback", "ascii__frontend_8py.html#adf568d8baca0701772280d0011e68a72", null ],
    [ "csv", "ascii__frontend_8py.html#a8c4ebdc20b35d7f4ede09b2cc3b26db0", null ],
    [ "file", "ascii__frontend_8py.html#a40a5d58ffa6e88aa578d6683ac413105", null ],
    [ "last_key", "ascii__frontend_8py.html#a9720411fe2b88d7e742f6e32871b5eac", null ],
    [ "length", "ascii__frontend_8py.html#af9d495c1655d813d553030485d00fea7", null ],
    [ "num", "ascii__frontend_8py.html#ac23fb295847684aaafd24830017a8180", null ],
    [ "run", "ascii__frontend_8py.html#a3e6f76368abfbc8709f4c1346dfa6a20", null ],
    [ "runs", "ascii__frontend_8py.html#ad51bedbd977a2b8a1f6d70460022215a", null ],
    [ "ser", "ascii__frontend_8py.html#a5b7028344d79661ed4abe535b0e7e307", null ],
    [ "split", "ascii__frontend_8py.html#a67f23769dfcb36d2355c8fa05dbf3b59", null ],
    [ "stripped", "ascii__frontend_8py.html#ae63f59673be6bdcfbcd32dd2d6942850", null ],
    [ "times", "ascii__frontend_8py.html#ac150111bafc331bafb353619452c5c5c", null ],
    [ "values", "ascii__frontend_8py.html#a7b6bc2414e0cc8dd1cb020560e19ad4f", null ]
];