var annotated_dup =
[
    [ "closed_loop", null, [
      [ "closedLoop", "classclosed__loop_1_1closedLoop.html", "classclosed__loop_1_1closedLoop" ]
    ] ],
    [ "controller", null, [
      [ "controller", "classcontroller_1_1controller.html", "classcontroller_1_1controller" ]
    ] ],
    [ "Elevator", null, [
      [ "Button", "classElevator_1_1Button.html", "classElevator_1_1Button" ],
      [ "MotorDriver", "classElevator_1_1MotorDriver.html", "classElevator_1_1MotorDriver" ],
      [ "TaskElevator", "classElevator_1_1TaskElevator.html", "classElevator_1_1TaskElevator" ]
    ] ],
    [ "encoder_driver", null, [
      [ "encoderDriver", "classencoder__driver_1_1encoderDriver.html", "classencoder__driver_1_1encoderDriver" ]
    ] ],
    [ "Lab0X03_Data_Collection", null, [
      [ "taskData", "classLab0X03__Data__Collection_1_1taskData.html", "classLab0X03__Data__Collection_1_1taskData" ]
    ] ],
    [ "led_fsm", null, [
      [ "taskBlinking", "classled__fsm_1_1taskBlinking.html", "classled__fsm_1_1taskBlinking" ]
    ] ],
    [ "main", null, [
      [ "Controller", "classmain_1_1Controller.html", "classmain_1_1Controller" ]
    ] ],
    [ "mcp9808", null, [
      [ "mcp9808", "classmcp9808_1_1mcp9808.html", "classmcp9808_1_1mcp9808" ]
    ] ],
    [ "motor_driver", null, [
      [ "motorDriver", "classmotor__driver_1_1motorDriver.html", "classmotor__driver_1_1motorDriver" ]
    ] ],
    [ "simon_taps", null, [
      [ "simonSays", "classsimon__taps_1_1simonSays.html", "classsimon__taps_1_1simonSays" ]
    ] ],
    [ "task_cipher", null, [
      [ "taskCipher", "classtask__cipher_1_1taskCipher.html", "classtask__cipher_1_1taskCipher" ]
    ] ],
    [ "think_fast", null, [
      [ "thinkFast", "classthink__fast_1_1thinkFast.html", "classthink__fast_1_1thinkFast" ]
    ] ],
    [ "think_fast_pt_B", null, [
      [ "thinkFastPtB", "classthink__fast__pt__B_1_1thinkFastPtB.html", "classthink__fast__pt__B_1_1thinkFastPtB" ]
    ] ],
    [ "touch_screen_driver", null, [
      [ "Touch", "classtouch__screen__driver_1_1Touch.html", "classtouch__screen__driver_1_1Touch" ]
    ] ],
    [ "Vendotron", null, [
      [ "vendingMachine", "classVendotron_1_1vendingMachine.html", "classVendotron_1_1vendingMachine" ]
    ] ]
];