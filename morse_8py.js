var morse_8py =
[
    [ "genWord", "morse_8py.html#a89aca1d00e735e66863fa34fae742caa", null ],
    [ "slow_type", "morse_8py.html#a2c8235f1b2e325115c1083d54ddfadd8", null ],
    [ "to_morse_code", "morse_8py.html#a883763bb8441b9dc3bedb42867b367f1", null ],
    [ "code", "morse_8py.html#a4c747141948b5e446d6cf802cae5f043", null ],
    [ "diff", "morse_8py.html#a6ff12b7d94b67fc587a093e681814f3b", null ],
    [ "easy", "morse_8py.html#a276f980bc157333c28d0021e2ff32cdb", null ],
    [ "hard", "morse_8py.html#abbc0a33a2d3d3f08a065fffbfdb2ff9d", null ],
    [ "practice", "morse_8py.html#afd481575297a8428940b644afb6f823b", null ],
    [ "splash", "morse_8py.html#a8d526e08f342cc4d75318fa2548def95", null ],
    [ "words", "morse_8py.html#a184f666c39a996330f3d27938ca2dd1b", null ]
];