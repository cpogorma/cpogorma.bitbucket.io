var classclosed__loop_1_1closedLoop =
[
    [ "__init__", "classclosed__loop_1_1closedLoop.html#ad8c97a3dfc47763de50791e4b67c480f", null ],
    [ "get_Kp", "classclosed__loop_1_1closedLoop.html#ad5f908d8002c364b2bc8974998fdb9be", null ],
    [ "run", "classclosed__loop_1_1closedLoop.html#a1942e0da4ddbf2038afa35259a00af9e", null ],
    [ "set_Kp", "classclosed__loop_1_1closedLoop.html#a733811d8919818bb865630ceb9b32419", null ],
    [ "effort_limit", "classclosed__loop_1_1closedLoop.html#a79a08a5b43492f96854ae633507214b0", null ],
    [ "kp", "classclosed__loop_1_1closedLoop.html#a72044a36b6c9a9a7c8381c2df90d1ae7", null ],
    [ "speed_des", "classclosed__loop_1_1closedLoop.html#a0d3af0737f0a61e2cafbd3bb96ccd951", null ]
];